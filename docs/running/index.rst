Configuration and OS booting
############################
.. toctree::
    :maxdepth: 2

    uefi_variables
    distro_installer
